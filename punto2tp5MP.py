#Utilizando listas diseñar un programa modular que permita gestionar los vehículos de un concesionario
#que se dedica a la venta de automóviles usados, las funcionalidades solicitadas son:
#a. Agregar vehículos, los atributos del mismo son:
#■ Dominio: String entre 6 y 9 caracteres sin espacios
#■ Marca: (R=Renault, F=Ford, C=Citroen)
#■ Tipo: U=Utilitario, A=Automóvil
#■ Modelo: en el rango [2005, 2020]
#■ Kilometraje
#■ Precio valuado: representa el precio de valuación del vehículo según su año de fabricación
#y estado.
#■ Precio de venta: Es de solo lectura y representa el precio de venta del vehículo y su valor
#es un 10% más que el precio valuado.
#■ Estado: (V=Vendido, D=Disponible, R=Reservado). Este valor no se debe ingresar, por
#defecto es disponible (D).
#b. Reservar un automóvil: implica cambiar el estado del vehículo a Reservado (R)
#c. Buscar un automóvil por su dominio
#d. Ordenar la lista de automóviles en forma ascendente o descendente por Marca
#e. Ordenar la lista de automóviles en forma ascendente o descendente por Precio de venta
#mostrando solamente los que se encuentren disponibles.
#Consideraciones
#- El programa debe cargar la lista en con al menos 10 automóviles en el momento en que se inicia.
#- Realice todas las validaciones necesarias para el punto 2) a)
import os
from operator import itemgetter,attrgetter

def menu():
    print('1)Agragar vehiculo')
    print('2)Reservar un automóvil')
    print('3)Buscar un automóvil por su dominio')
    print('4)Ordenar la lista de automóviles en forma ascendente o descendente por Marca')
    print('5)Ordenar la lista de automóviles en forma ascendente o descendente por Precio de venta')
    print('6)salir')
    opc=int(input(' Eliga una opcion'))
    while opc<1 or opc>6 :
        print('opcion no valida, intentelo otra vez')
        opc=int(input(' Eliga una opcion'))
    return opc


""""
lista de autos[dominio,marca     ,tipo          ,modelo,kilometraje,preciovaluado,presioventa,estado]
                       renault    utillitario    2005                                           resesvado
                       ford        automovil    .....                                             vendido
                       citreon                   2020                                              disponible
"""


def simodelo():
   modelo=int(input('Ingrese año del modelo [2005, 2020]: '))
   while modelo<2005 or modelo>2020:
        print('modelo no valido,intentelo una vez mas')
        modelo=int(input('Ingrese año del modelo [2005, 2020]: ') )
   return modelo

def simarca():
    marca=str(input('Ingrese la marca del automovil R=Renault, F=Ford, C=Citroen: '))
    while marca not in marcas:
        print('la marca no es valida intentelo una vez mas')
        marca=str(input('Ingrese la marca del automovil R=Renault, F=Ford, C=Citroen: '))
    return marca

def sitipo():
    tipo=str(input('Seleccione el tipo (U=Utilitario, A=Automóvil): '))
    while tipo not in tipos :
        print('El tipo ingresado no es valido, intentelo una ve mas')
        tipo=str(input('Seleccione el tipo (U=Utilitario, A=Automóvil): '))
    return tipo

def agregar(listamadre):
    dominio=str(input('Dominio: '))
    kilometraje=int(input('indeique el kilometraje'))
    precioValuado=float(input('ingrese el precio valuado del vehiculo'))
    precioVenta=(precioValuado+((precioValuado*10)/100))
    print('Precio de Venta: ',precioVenta)
    modelo=simodelo()
    marca=simarca()
    tipo=sitipo()
    estado=estados[1]
    listamadre.append([dominio,kilometraje,precioValuado,precioVenta,modelo,marca,tipo,estado])
    return listamadre
#b. Reservar un automóvil: implica cambiar el estado del vehículo a Reservado (R)
def buscar(listamadre):
    pos=-1
    Abuscar=str(input('indique el domio del auto que desea : '))
    for i,dato in enumerate(listamadre):
        if dato[0]==Abuscar:
            pos=i
    return pos
# Estado: (V=Vendido, D=Disponible, R=Reservado
def reservar(listamadre):
    pos=buscar(listamadre)
    if pos==-1:
        print('no se encontro el automovil con ese dominio')
    else :
        if listamadre [pos][7]=='D':
            listamadre[pos][7]='R'
        else :
            print('el automovil fue vendido o ya esta reservado')
#c. Buscar un automóvil por su dominio
def buscarauto (listamadre):
    pos=buscar(listamadre)
    print('detalles del auto buscado: ',listamadre[pos])

#d. Ordenar la lista de automóviles en forma ascendente o descendente por Marca
def ascendente(listamadre):
    ordenado=sorted(listamadre, key=itemgetter(5))
    print(ordenado)

#Ordenar la lista de automóviles en forma ascendente o descendente por Precio de venta
#mostrando solamente los que se encuentren disponibles.
def menuAD():
    print('1)Ordenar de forma decreciente por precio/ D ')
    print('2)Ordenar de forma ascendete por el precio/ A')
    opci=int(input('eliga una opcion: '))
    while opci<1 or opci>2  :
        print('opcion no valida, intentelo una vez mas')
        opci=int(input('eliga una opcion: '))
    return opci
def elegirAD(listamadre):
    opci=menuAD()
    if opci==1:
        print(sorted(listamadre,reverse=False,key=itemgetter(3)))
    elif opci==2:
          print(sorted(listamadre,reverse=True,key=itemgetter(3)))

# listamadre=[dominio,kilometraje,precioValuado,precioVenta,modelo,marca,tipo,estado]
#V=Vendido, D=Disponible, R=Reservado
listamadre=[['casa100',1984565,45000,49500,2005,'F','A','D'],#0
            ['casa101',1984565,45100,49501,2005,'R','A','D'],#1
            ['casa102',1984565,45200,49502,2005,'R','A','R'],#2
            ['casa103',1984565,45300,49503,2005,'F','A','D'],#3
            ['casa104',1984565,45400,49504,2005,'R','A','D'],#4
            ['casa105',1984565,45500,49505,2005,'R','A','D'],#5
            ['casa106',1984565,45600,49506,2005,'C','U','V'],#6
            ['casa107',1984565,45700,49507,2005,'R','A','D'],#7
            ['casa108',1984565,45800,49508,2005,'R','A','D'],#8
            ['casa109',1984565,45900,49509,2005,'C','A','D'],#9
            ['casa110',1984565,45110,49510,2020,'R','U','D']]#10
tipos=['U','A']
estados=['V','D','R']
marcas=['R','F','C']
opc=0
while opc!=6 :
    opc=menu()
    if opc==1 :
        agregar(listamadre)
    elif opc==2:
       reservar(listamadre)
    elif opc==3:
        buscarauto (listamadre)
    elif opc==4 :
        ascendente(listamadre)
    elif opc == 5:
        elegirAD(listamadre)
    elif opc ==6 :
        print('fin del programa')







