import os
from typing import DefaultDict

def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('cls')

def menu():
    print('1) Registrar productos')
    print('2) Mostrar el listado de productos')
    print('3) Eliminar productos con stock igual a 0 ')
    print('4) Mostrar productos con stock entre [A,B]')
    print('5) Salir')
    eleccion = int(input('Elija una opción: '))
    while not((eleccion >= 1) and (eleccion <= 5)):
        eleccion = int(input('Elija una opción: '))
    os.system('cls')
    return eleccion
#Registrar productos: para cada uno se debe solicitar, código, descripción, precio y stock. Agregar las
#siguientes validaciones:
#i. El código no se puede repetir
#ii. Todos los valores son obligatorios
#iii. El precio y el stock no pueden ser negativos

#revisar q no sean negativos
def nonegativo():
    valorn= float(input('valor: '))
    while (valorn < 0):
        valorn= float(input('valor:  '))
    return valorn

#caragar cosas
def registro():
    print('Cargar Lista de Productos')
    productos = {}
    codigo = -1
    while (codigo != 0):
        codigo = int(input('(coloque "0" (cero) para finalizar): '))
        if codigo != 0: 
            if codigo not in productos:    
                descripcion = input('descripcion: ')
                print('---stock disponible:---- ')
                stock = nonegativo()
                print('----precio:$-----')
                precio = nonegativo()
                productos[codigo] = [descripcion,stock,precio]
                print('agregado correctamente')
            else:
                print('el proucto ya existe')

    return productos
#Mostrar
def mostrar(diccionario):
    print('Listado de Productos')
    for clave, valor in diccionario.items():
        print('codigo',clave,'descripcion, precio, stock',valor)


#seleccion intervalo
#Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta

def buscarEnIntervalo(productos):
    print('Lista de productos con stock entre el intervalo: ')
    print('desde "A"')
    a = nonegativo()
    print('hasta "B"')
    b= nonegativo()
    suma = 0.
    for stock, datos in productos.items():
            if (datos[1] >= a) & (datos[1] <= b): 
                print(' Producto con estock entre el intervalo[',a,',',b,']')
                print(stock, datos)
                suma += datos[1]
            else :
                print ('no se encontro ')
#Diseñar un proceso que le sume X al stock de todos los productos 
# cuyo valor actual de stock sea menor al valor Y


#Eliminar todos los productos cuyo stock sea igual a cero.

def eliminar(productos):
    claves = list(productos.keys())
    print(claves)
    valores=list(productos.values())
    for stock, datos in valores.items():
        if (datos[1] == 0) : 
                print(' Producto con estock igual a 0')
                print(stock, datos)
    
  
     
#menu de seleccion de opciones

def salir():
    print('Fin del programa...')

#principal
opcion = 0

os.system('cls')
while (opcion != 7):
    opcion = menu()
    if opcion == 1:
        productos = registro()
    elif opcion == 2:
        mostrar(productos)
    elif opcion == 3:
        eliminar(productos)
    elif opcion == 4:
        buscarEnIntervalo(productos)
    elif (opcion == 5):        
        salir()
    continuar()